"""Application settings."""

from pydantic_settings import BaseSettings, SettingsConfigDict


class AppSettings(BaseSettings):
    model_config = SettingsConfigDict(
        env_file=".env", env_file_encoding="utf-8", extra="ignore"
    )

    # base kwargs
    DEBUG: bool = False

    # database
    SQL_DEBUG: bool = True
    POSTGRES_DSN: str = ""
    POSTGRES_DB_SCHEME: str = "dbo"

    APP_HOST: str = ""
    APP_PORT: int = 8010

    # JWT
    JWT_SECRET_KEY: str = ""
    JWT_EXPIRATION_TIME: int = 3600
    JWT_ALGORITHM: str = ""


settings = AppSettings()
