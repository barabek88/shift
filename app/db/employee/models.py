"""Database models declarations."""

from sqlalchemy import Integer, String
from sqlalchemy.orm import Mapped, mapped_column

from app.db.sqlalchemy import Base
from app.settings import settings


class EmployeeModel(Base):
    __tablename__ = "employee"

    __table_args__ = {"schema": settings.POSTGRES_DB_SCHEME}

    id: Mapped[int] = mapped_column(Integer, primary_key=True, autoincrement=True)
    login: Mapped[str] = mapped_column(String(200))  # noqa: WPS432
    password: Mapped[str] = mapped_column(String(200))  # noqa: WPS432
