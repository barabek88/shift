"""Database operations for audio processing service."""

from typing import Any, Dict

from app.db.crud import CRUD
from app.db.employee.models import EmployeeModel
from app.db.sqlalchemy import AsyncSession
from app.schemas.employee import Employee


class EmployeeRepo:
    def __init__(self, session: AsyncSession):
        self._crud = CRUD(session=session, cls_model=EmployeeModel)

    async def create_employee(self, model_data: Dict[str, Any]) -> int:
        """Create record in db."""
        return await self._crud.create(
            model_data=model_data,
        )

    async def get_employee_by_login(self, login: str) -> Employee | None:
        """Return record row in db."""
        employee_in_db = await self._crud.get_or_none_by_unique_field(
            field="login",
            field_value=login,
        )

        if employee_in_db:
            return self._to_domain(employee_in_db)

        return None

    def _to_domain(self, employee_in_db: EmployeeModel) -> Employee:
        return Employee(
            id=employee_in_db.id,
            login=employee_in_db.login,
            password=employee_in_db.password,
        )
