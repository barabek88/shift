"""Database models declarations."""

from sqlalchemy import DateTime, ForeignKey, Integer, Numeric
from sqlalchemy.orm import Mapped, mapped_column, relationship

from app.db.employee.models import EmployeeModel
from app.db.sqlalchemy import Base
from app.settings import settings

DB_SCHEMA = settings.POSTGRES_DB_SCHEME


class EmployeeDataModel(Base):
    __tablename__ = "employee_data"

    __table_args__ = {"schema": DB_SCHEMA}

    id: Mapped[int] = mapped_column(Integer, primary_key=True, autoincrement=True)
    employee_id: Mapped[int] = mapped_column(
        ForeignKey(f"{DB_SCHEMA}.employee.id"),
    )
    employee: Mapped["EmployeeModel"] = relationship(lazy=True)
    promotion_date_at: Mapped[DateTime] = mapped_column(
        DateTime(timezone=True),
        nullable=True,
    )
    salary: Mapped[float] = mapped_column(
        Numeric(precision=20, scale=3), nullable=True  # noqa: WPS432
    )
