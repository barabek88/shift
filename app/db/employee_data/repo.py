"""Database operations for audio processing service."""

from typing import Any, Dict

from sqlalchemy import select

from app.db.crud import CRUD
from app.db.employee.models import EmployeeModel
from app.db.employee_data.models import EmployeeDataModel
from app.db.sqlalchemy import AsyncSession
from app.schemas.employee_data import EmployeeData


class EmployeeDataRepo:
    def __init__(self, session: AsyncSession):
        self._session = session
        self._crud = CRUD(session=session, cls_model=EmployeeDataModel)

    async def create_employee_data(self, model_data: Dict[str, Any]) -> int:
        """Create record in db."""
        return await self._crud.create(
            model_data=model_data,
        )

    async def update_employee_data(
        self, row_id: int, model_data: Dict[str, Any]
    ) -> int:
        """Update record row in db."""
        await self._crud.update(
            pkey_val=row_id,
            model_data=model_data,
        )

        return row_id

    async def get_record_by_employee_id(self, employee_id: int) -> EmployeeData | None:
        """Return record row in db."""
        employee_data_in_db = await self._crud.get_or_none_by_unique_field(
            field="employee_id",
            field_value=employee_id,
        )

        if employee_data_in_db:
            return self._to_domain(employee_data_in_db)

        return None

    async def get_record_by_login(self, login: str) -> EmployeeData | None:
        """Return record row in db."""

        query = (
            select(EmployeeDataModel, EmployeeModel.login)
            .join(EmployeeDataModel.employee)
            .filter(EmployeeModel.login == login)
        )

        rows = await self._session.execute(query)

        employee_data_in_db = rows.scalar()

        if employee_data_in_db:
            return self._to_domain(employee_data_in_db)

        return None

    def _to_domain(self, employee_data_in_db: EmployeeDataModel) -> EmployeeData:
        return EmployeeData(
            id=employee_data_in_db.id,
            employee_id=employee_data_in_db.employee_id,
            promotion_date=employee_data_in_db.promotion_date_at,
            salary=employee_data_in_db.salary,
        )
