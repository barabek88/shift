"""Endpoint healthcheck."""

from fastapi import APIRouter, Depends, Response, status
from fastapi.responses import JSONResponse
from sqlalchemy.ext.asyncio import AsyncSession

from app.db import sqlalchemy as db
from app.schemas.login import LoginReq
from app.services.login.login import get_token

router = APIRouter(tags=["Auth"])


@router.post("/login")
async def login(
    login_req: LoginReq,
    database: AsyncSession = Depends(db.get_db),
) -> Response:
    token = await get_token(
        login_req.login,
        login_req.password,
        database,
    )

    return JSONResponse(
        status_code=(status.HTTP_200_OK),
        content={"token": token},
    )
