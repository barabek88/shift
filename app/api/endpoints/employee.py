"""Endpoint healthcheck."""

from fastapi import APIRouter, Depends, Response, status
from fastapi.responses import JSONResponse
from sqlalchemy.ext.asyncio import AsyncSession

from app.db import sqlalchemy as db
from app.schemas.employee import EmployeeReq
from app.services.employee.employee import create_employee

router = APIRouter(tags=["Employee"], prefix="/employee")


@router.post("")
async def create(
    employee_req: EmployeeReq,
    database: AsyncSession = Depends(db.get_db),
) -> Response:

    employee_id = await create_employee(
        employee_req.login,
        employee_req.password,
        database,
    )

    return JSONResponse(
        status_code=(status.HTTP_201_CREATED),
        content={"id": employee_id},
    )
