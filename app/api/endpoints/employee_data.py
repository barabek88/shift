"""Endpoint healthcheck."""

from fastapi import APIRouter, Depends, Response, status
from fastapi.responses import JSONResponse
from sqlalchemy.ext.asyncio import AsyncSession

from app.api.dependencies.get_current_login import get_current_login_dependency
from app.db import sqlalchemy as db
from app.exceptions import AppError
from app.resources import strings
from app.schemas.employee_data import EmployeeDataReq
from app.services.employee_data.employee_data import (
    get_employee_promotion_date,
    get_employee_salary,
    update_employee_data,
)

router = APIRouter(tags=["Employee Data"], prefix="/employee-data")


@router.get("/{login}/promotion-date")
async def get_promotion_date(
    login: str,
    database: AsyncSession = Depends(db.get_db),
) -> Response:

    promotion_date = await get_employee_promotion_date(login, database)

    return JSONResponse(
        status_code=(status.HTTP_200_OK),
        content={"promotion_date": promotion_date},
    )


@router.get("/{login}/salary")
async def get_salary(
    login: str,
    current_login: str | None = get_current_login_dependency,
    database: AsyncSession = Depends(db.get_db),
) -> Response:
    if current_login != login:
        raise AppError(strings.ACTION_DENIED, status.HTTP_403_FORBIDDEN)

    salary = await get_employee_salary(login, database)

    return JSONResponse(
        status_code=(status.HTTP_200_OK),
        content={"salary": salary},
    )


@router.post("")
async def create(
    employee_req: EmployeeDataReq,
    database: AsyncSession = Depends(db.get_db),
) -> Response:

    employee_data_id = await update_employee_data(
        employee_req.login,
        employee_req.salary,
        employee_req.promotion_date,
        database,
    )

    return JSONResponse(
        status_code=(status.HTTP_200_OK),
        content={"id": employee_data_id},
    )
