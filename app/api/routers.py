"""Configuration of routers for all endpoints."""

from fastapi import APIRouter

from app.api.endpoints.auth import router as auth_router
from app.api.endpoints.employee import router as employee_router
from app.api.endpoints.employee_data import router as employee_data_router

router = APIRouter()

router.include_router(employee_router)
router.include_router(employee_data_router)
router.include_router(auth_router)
