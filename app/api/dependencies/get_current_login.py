"""Provides login dependency for service."""

from fastapi import Depends, Request, status

from app.exceptions import AppError
from app.resources import strings
from app.settings import settings
from app.utils.jwt_token import get_data_from_jwt_token


async def get_current_login(request: Request) -> str | None:
    authorization = request.headers.get("authorization")

    if not authorization or not authorization.startswith("Bearer "):
        raise AppError(strings.TOKEN_MISSING, status.HTTP_401_UNAUTHORIZED)

    token = authorization.split(" ")[1]

    payload = get_data_from_jwt_token(
        token, settings.JWT_SECRET_KEY, settings.JWT_ALGORITHM
    )

    if payload:
        return payload.get("login")

    return None


get_current_login_dependency = Depends(get_current_login)
