"""Provides service with auxiliary operations."""

import bcrypt
from sqlalchemy.ext.asyncio import AsyncSession

from app.db.employee.repo import EmployeeRepo
from app.exceptions import AppError
from app.logger import logger
from app.resources import strings


async def create_employee(
    login: str,
    password: str,
    database: AsyncSession,
) -> int:
    """Create new employee."""
    logger.info("Вход в обработку ендпоинта - создание работника", login)

    if not login or not password:
        raise AppError(strings.LOGIN_OR_PASS_IS_EMPTY)

    repo = EmployeeRepo(database)

    employee = await repo.get_employee_by_login(login)

    if employee:
        raise AppError(strings.LOGIN_IS_ALREADY_CREATED)

    hashed_password = bcrypt.hashpw(password.encode("utf-8"), bcrypt.gensalt()).decode(
        "utf-8"
    )

    employee_model = {"login": login, "password": hashed_password}

    return await repo.create_employee(employee_model)
