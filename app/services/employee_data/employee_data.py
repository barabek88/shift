"""Provides service with auxiliary operations."""

from datetime import datetime

from fastapi import status
from sqlalchemy.ext.asyncio import AsyncSession

from app.db.employee.repo import EmployeeRepo
from app.db.employee_data.repo import EmployeeDataRepo
from app.exceptions import AppError
from app.logger import logger
from app.resources import strings


async def update_employee_data(
    login: str,
    salary: float,
    promotion_date: datetime,
    database: AsyncSession,
) -> int:
    """Create or update employee data."""

    logger.info(
        "Вход в обработку ендпоинта - создание/обновление информации о работнике",
        login,
        promotion_date,
    )

    if not login or not salary or not promotion_date:
        raise AppError(strings.LOGIN_OR_SALARY_OR_DATE_IS_EMPTY)

    employee_repo = EmployeeRepo(database)

    employee = await employee_repo.get_employee_by_login(login)

    if not employee:
        raise AppError(strings.NOT_FOUND_ERROR_MSG, status.HTTP_404_NOT_FOUND)

    employee_data_repo = EmployeeDataRepo(database)
    employee_data_model = {
        "salary": salary,
        "promotion_date_at": promotion_date,
        "employee_id": employee.id,
    }

    employee_data = await employee_data_repo.get_record_by_employee_id(employee.id)

    if not employee_data:
        return await employee_data_repo.create_employee_data(employee_data_model)

    return await employee_data_repo.update_employee_data(
        employee_data.id, employee_data_model
    )


async def get_employee_promotion_date(
    login: str,
    database: AsyncSession,
) -> str | None:
    """Return employee promotion_date."""
    logger.info(
        "Вход в обработку ендпоинта - получение даты повышения сотрудника",
        login,
    )

    if not login:
        raise AppError(strings.LOGIN_IS_EMPTY)

    employee_data_repo = EmployeeDataRepo(database)

    employee_data = await employee_data_repo.get_record_by_login(login)

    if not employee_data:
        raise AppError(strings.NOT_FOUND_ERROR_MSG, status.HTTP_404_NOT_FOUND)

    if employee_data.promotion_date:
        return employee_data.promotion_date.strftime("%Y-%m-%d")

    return None


async def get_employee_salary(
    login: str,
    database: AsyncSession,
) -> float | None:
    """Return employee salary."""
    logger.info(
        "Вход в обработку ендпоинта - получение зарплаты сотрудника",
        login,
    )

    if not login:
        raise AppError(strings.LOGIN_IS_EMPTY)

    employee_data_repo = EmployeeDataRepo(database)

    employee_data = await employee_data_repo.get_record_by_login(login)

    if not employee_data:
        raise AppError(strings.NOT_FOUND_ERROR_MSG, status.HTTP_404_NOT_FOUND)

    return employee_data.salary
