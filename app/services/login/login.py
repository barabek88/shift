"""Provides login service."""

import bcrypt
from fastapi import status
from sqlalchemy.ext.asyncio import AsyncSession

from app.db.employee.repo import EmployeeRepo
from app.exceptions import AppError
from app.logger import logger
from app.resources import strings
from app.settings import settings
from app.utils.jwt_token import generate_jwt_token


async def get_token(
    login: str,
    password: str,
    database: AsyncSession,
) -> str:
    """Create token for the employee."""
    logger.info("Вход в обработку ендпоинта - создание токена для сотрудника", login)

    if not login or not password:
        raise AppError(strings.LOGIN_OR_PASS_IS_EMPTY)

    repo = EmployeeRepo(database)

    employee = await repo.get_employee_by_login(login)

    if not employee:
        raise AppError(strings.NOT_FOUND_ERROR_MSG, status.HTTP_404_NOT_FOUND)

    is_password_valid = bcrypt.checkpw(
        password.encode("utf-8"),
        employee.password.encode("utf-8"),
    )

    if not is_password_valid:
        raise AppError(strings.PASSWORD_IS_NOT_VALID)

    token_data = {
        "login": login,
    }

    return generate_jwt_token(
        token_data,
        settings.JWT_SECRET_KEY,
        settings.JWT_ALGORITHM,
        settings.JWT_EXPIRATION_TIME,
    )
