"""This module defines the Employee schema model."""

from datetime import datetime

from pydantic import BaseModel


class EmployeeData(BaseModel):
    id: int
    employee_id: int
    promotion_date: datetime | None = None
    salary: float | None = None


class EmployeeDataReq(BaseModel):
    login: str
    promotion_date: datetime
    salary: float
