"""This module defines the Employee schema model."""

from pydantic import BaseModel


class Employee(BaseModel):
    id: int
    login: str
    password: str


class EmployeeReq(BaseModel):
    login: str
    password: str
