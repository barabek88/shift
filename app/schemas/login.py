"""This module defines the Login schema model."""

from pydantic import BaseModel


class LoginReq(BaseModel):
    login: str
    password: str
