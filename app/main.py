"""App main class."""

import uvicorn
from fastapi import FastAPI, Request
from fastapi.responses import JSONResponse

from app.api.routers import router
from app.exceptions import AppError
from app.logger import logger
from app.resources import strings
from app.settings import settings

app = FastAPI()


app.include_router(router)


@app.exception_handler(AppError)
async def app_exception_handler(request: Request, exc: AppError) -> JSONResponse:
    """App exception handler."""
    error_message = strings.APP_EXCEPTION_TEMPLATE.format(message=exc.name)

    logger.error(error_message)

    return JSONResponse(status_code=exc.status_code, content={"message": error_message})


if __name__ == "__main__":
    uvicorn.run(app, host=settings.APP_HOST, port=settings.APP_PORT)
