run-pre-commit:
	poetry run bash ./scripts/format && \
	poetry run bash ./scripts/lint && \
	poetry run bash ./scripts/test tests

run-tests:
	poetry run bash ./scripts/test tests
